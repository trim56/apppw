//
//  Extension.swift
//  AppPW
//
//  Created by Павел Зорин on 07/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

extension UIColor {
    
    // MARK: - Для преобразования HEX в RGB
    convenience init?(hex: String) {
        let hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        
        var rgb: UInt32 = 0
        
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        let length = hexSanitized.count
        
        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }
        
        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0
            
        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0
            
        } else {
            return nil
        }
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
    
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - Для преобразования RGB в HEX
    
    func toHex(alpha: Bool = false) -> String? {
        
        guard let components = cgColor.components, components.count >= 2 else {
            return nil
        }
        
        var r = Float(components[0])
        var g = Float(components[1])
        var b = Float(1.0)
        var a = Float(1.0)
        
        if components.count == 2 {
            r = Float(components[0])
            g = Float(components[0])
            b = Float(components[0])
        }
        
        if components.count >= 3 {
            b = Float(components[2])
        }
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
}

extension UITextField {
    
    // MARK: - Рисует рамку внизу текстового поля
    func setBottomBorder(color: UIColor) {
        
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = color.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    // MARK: - Меняем цвет Placeholder текстового поля
    func placeholderColor(color: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,attributes: [NSAttributedString.Key.foregroundColor: color])
    }
}

extension UIView {
     // Удаляет все дочернии представления
    func removeAllSubViews() {
        self.subviews.forEach({ $0.removeFromSuperview() })
    }
    
}

extension String {
    // Валидация строки на соответствие формату Email
    func isValidEmail() -> Bool {
        
        let exp: String = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
            "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
            "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let regex = try! NSRegularExpression(pattern: exp, options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
        
    }
    // Приводит строку к денежному формату
    func moneyFormat() -> String {
        var string = self
        let formatter = NumberFormatter()
        if let number = formatter.number(from: self) {
            formatter.numberStyle = .currency
            formatter.locale = Locale.current
            formatter.currencySymbol = ""
            string = formatter.string(from: number)!
        }
        return string
        
    }
    
}

extension NSNumber {
    // Номер в строку в денежном формате
    var moneyAmount: String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencySymbol = ""
        return formatter.string(from: self)!
    }
}

extension CGFloat{
    // Строку в Дробь
    init(_ str:String? = nil){
        guard
            let s = str,
            let n = NumberFormatter().number(from: s) else {
                self = 0.0
                return
        }
        self = CGFloat(truncating: n)
    }
}

extension String {
    
    var stringToNumber: Double {
        guard
            let n = Double(self) else {
                return 0.0
        }
        return n
    }
    
    // Изменение формата даты
    func formatToString(format: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "yyyy-M-dd HH-mm-ss"
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = format
        if let date = dateFormatter.date(from: self) {
            return newFormatter.string(from: date)
        }else {
            return nil
        }
    }
    // В Unixtimestamp
    func formatToDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .iso8601)
        dateFormatter.dateFormat = "yyyy-M-dd HH-mm-ss"
        if let date = dateFormatter.date(from: self) {
            return date
        }else {
            return nil
        }
    }
}
