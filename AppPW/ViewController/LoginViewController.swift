//
//  ViewController.swift
//  AppPW
//
//  Created by Павел Зорин on 06/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var signInForm: SignInForm!
    @IBOutlet weak var registerForm: RegisterForm!
    @IBOutlet weak var contentViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var signInBorder: UIView!
    @IBOutlet weak var registerBorder: UIView!

    // Высота клавиатуры
    var keyboardHeight: CGFloat!
    // Верхняя точка расположения текствого поля
    var lastIndent: CGFloat!
    // Прелоад
    var preload: PreloadView!
    
    
    override func viewWillDisappear(_ animated: Bool) {
        // Деактивируем, что бы не глюкануло
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Для клавиатуры
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // Устанавливаем делегаты для всех текстовых полей
        signInForm.emailField.delegate = self
        signInForm.passwordField.delegate = self
        registerForm.usernameField.delegate = self
        registerForm.emailField.delegate = self
        registerForm.passwordField.delegate = self
        registerForm.confirmPasswordField.delegate = self
        
        // События на кнопки
        signInForm.signInButton.addTarget(self, action: #selector(signInAction), for: .touchUpInside)
        registerForm.registerButton.addTarget(self, action: #selector(registerAction), for: .touchUpInside)

        
    }
    
    // Метод установки стиля StatusBar
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .default
    }
    
    // Переключение на форму Вход в систему
    @IBAction func signInTap(_ sender: UIButton) {
        self.view.endEditing(true)
        // Первый этам анимации перекрашивает и скрывает деактивируемый блок формы
        UIView.animate(withDuration: ANIMATE_DURATION, animations: {
            
            self.registerButton.setTitleColor(DISABLED_COLOR, for: .normal)
            self.registerBorder.backgroundColor = DISABLED_COLOR
            self.registerForm.alpha = 0
            
        }, completion: { (value: Bool) in
            
            self.registerForm.isHidden = true
            self.signInForm.isHidden = false
            
            // Второй этам анимации перекрашивает и скрывает активируемый блок формы
            UIView.animate(withDuration: ANIMATE_DURATION, animations: {
                
                self.signInButton.setTitleColor(ACTIVE_COLOR, for: .normal)
                self.signInBorder.backgroundColor = ACTIVE_COLOR
                self.signInForm.alpha = 1
            })
        })
    }
    
    // Переключение на форму Регистрация
    @IBAction func registerTap(_ sender: UIButton) {
        self.view.endEditing(true)
        UIView.animate(withDuration: ANIMATE_DURATION, animations: {
            
            self.signInButton.setTitleColor(DISABLED_COLOR, for: .normal)
            self.signInBorder.backgroundColor = DISABLED_COLOR
            self.signInForm.alpha = 0
            
        }, completion: { (value: Bool) in
            
            self.signInForm.isHidden = true
            self.registerForm.isHidden = false
            
            UIView.animate(withDuration: ANIMATE_DURATION, animations: {
                
                self.registerButton.setTitleColor(ACTIVE_COLOR, for: .normal)
                self.registerBorder.backgroundColor = ACTIVE_COLOR
                self.registerForm.alpha = 1
            })
        })
    }
    
    // Скрывает клавиатуру при любом нажатии на экран
    @IBAction func onTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    // При активации клавиатуры вычисляется её высота и производим попытку движения представления
    @objc func keyboardWillShow(notification:NSNotification) {
        if self.keyboardHeight == nil {
            if let userInfo = notification.userInfo {
                self.keyboardHeight = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue.height
                adjustingHeight(show: true)
            }
        }
        
    }
    
    // Отрабатывает при сворачивании клавиатуры
    @objc func keyboardWillHide(notification:NSNotification) {
        adjustingHeight(show: false)
    }
    
    // Двигает представление контента относительно высоты клавиатуры и местоположения текстового поля
    private func adjustingHeight(show: Bool) {
        
        // Если на текущий момент нет значения высоту клавиатуры, то выход
        if self.keyboardHeight == nil {
            return
        }
        
        // Вычисляем высоту экрана
        let content = self.view.frame.size.height

        // Вычисляем относительную точку расположения текстового поля
        // Расчет: высота представления - высота клавиатуры - верхняя точка расположения текствого поля - максимальный нижний отступ от текстового поля
        var point = (content - self.keyboardHeight - self.lastIndent - 150)
        // Если нужно двигать
        if show {
            // Проверяем не положительный ли
            if point > 0 {
                // Тогда ни чего не делаем так как поле и так достаточно высоко над клавиатурой
                return
            }
        // Если нужно вернуть на исходную
        }else {
            point = 0
        }

        // Конечное условие направление и двигаем представление
        let changeInHeight = point * (show ? 1 : -1)
        self.contentViewTop.constant = changeInHeight
        
    }
    
    // Авторизация
    @objc private func signInAction(){
        
        // Такого не должно быть, но на всякий случай)
        guard let email = signInForm.emailField.text,
            let password = signInForm.passwordField.text
        else {
            return
        }
        // Скрываем клавиатуру
        view.endEditing(true)
        // Запускаем прелоад
        self.preload = PreloadView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height), delegatView: self.view)
        // Валидация
        let msg = Validate().check(fields: ["email": email, "password": password])
        if msg != "" {
            preload.setAlert(message: msg)
            return
        }
        // Делаем запрос
        Network().connect(method: "POST", actionType: "login", params: ["email": email, "password": password]) {results in
            self.checkNetworkresult(results: results)
        }
        
    }
    
    // Регистрация
    @objc private func registerAction(){
        
        // Такого не должно быть, но на всякий случай)
        guard let username = registerForm.usernameField.text,
            let email = registerForm.emailField.text,
            let password = registerForm.passwordField.text,
            let confirmPassword = registerForm.confirmPasswordField.text
            else {
                return
        }
        // Скрываем клавиатуру
        view.endEditing(true)
        // Запускаем прелоад
        self.preload = PreloadView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height), delegatView: self.view)
        // Валидация
        let msg = Validate().check(fields: ["username": username, "email": email, "password": password, "confirmPassword": confirmPassword])
        if msg != "" {
            preload.setAlert(message: msg)
            return
        }
        // Делаем запрос
        Network().connect(method: "POST", actionType: "create", params: ["username": username, "email": email, "password": password]) {results in
            self.checkNetworkresult(results: results)
        }
        
    }
    
    // Проверка результата запроса
    private func checkNetworkresult(results: Dictionary<String, AnyObject>){
        
        if (results["status"] as! Bool) {
            // Если поле токена есть
            if let token = results["data"]!["id_token"] {
                // Пихаем в настройки пользователя
                let userDefaults = UserDefaults.standard
                userDefaults.set(token, forKey: TOKEN)
                userDefaults.synchronize()
                
                // Убиваем прелоад
                self.preload.removeFromSuperview()
                // Отправляемся в главное меню
                self.goMenu()
                
            }else {
                // Заменяем прелоад окном об ошибке
                self.preload.setAlert(message: NSLocalizedString("FAILED_TOKEN", comment: ""))
            }
            
        }else {
            // Заменяем прелоад окном об ошибке
            self.preload.setAlert(message: results["message"] as! String)
        }
        
    }
    
    // Запуск экрано главного меню
    private func goMenu(){
        
        UIView.animate(withDuration: ANIMATE_DURATION, animations: {
            
            self.scrollView.alpha = 0
            
        }, completion: { (value: Bool) in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "MainPage")
            self.present(controller, animated: true, completion: nil)
            
        })
    }

}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // Вычисляем верхнюю точка расположения текствого поля
        self.lastIndent = textField.convert(textField.bounds, to: self.view).origin.y
        // Если на текущий момент есть значение высоты клавиатуры, то переходим к расчету движения
        if self.keyboardHeight != nil {
            adjustingHeight(show: true)
        }
        textField.setBottomBorder(color: ACTIVE_COLOR)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        adjustingHeight(show: false)
        // Вычисляем верхнюю точка расположения текствого поля
        self.lastIndent = textField.convert(textField.bounds, to: self.view).origin.y
        textField.setBottomBorder(color: DISABLED_COLOR)
        return true
    }
}

