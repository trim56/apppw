//
//  MainViewController.swift
//  AppPW
//
//  Created by Павел Зорин on 07/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, HistoryTableDelegate {
    
    // Реализация метода Делегата
    // ДУБЛЯЖ КОДА :(
    // ОЧЕНЬ плохо запрос в запросе
    func repeatTransfer(data: [String : String]) {
        
        let recipient = data["username"]!
        let amount = data["amount"]!
        
        self.preload = PreloadView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height), delegatView: self.view)
        // Валидация
        let msg = Validate().check(fields: ["amount": amount, "recipient": recipient, "balance": self.userBalance])
        if msg != "" {
            preload.setAlert(message: msg)
            return
        }
        // Делаем запрос
        Network().connect(method: "POST", actionType: "createTransaction", params: ["name": recipient, "amount": amount], token: true) {results in
            
            if (results["status"] as! Bool) {
                
                let data = results["data"]?["trans_token"] as? [String: AnyObject]
                
                let row = GetDataRow(data: data!)
                // Обновляем данные в интерфейсе
                self.balanceTitle.text = row.balance
                
                // Пишем используемые данные пользователя
                self.userBalance = row.balanceDouble
                
                Network().connect(method: "GET", actionType: "listTransactions", token: true) {results in
                    if (results["status"] as! Bool) {
                        
                        self.transferView = nil
                        self.underContentView.removeAllSubViews()
                        
                        let data = results["data"]?["trans_token"] as? [[String : AnyObject]]
                        
                        self.historyView = HistoryTable(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height), data: data!)
                        self.underContentView.addSubview(self.historyView)
                        
                        self.preload.setAlert(message: NSLocalizedString("TRANSACTION_DONE", comment: ""))
                        
                    }else {
                        // Заменяем прелоад окном об ошибке
                        self.preload.setAlert(message: results["message"] as! String)
                    }
                    
                }
            }else {
                // Заменяем прелоад окном об ошибке
                self.preload.setAlert(message: results["message"] as! String)
            }
            
        }
    }
    

    @IBOutlet weak var balanceTitle: UILabel!
    @IBOutlet weak var nameTitle: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var underContentView: UIView!
    @IBOutlet weak var buttonTransfer: UIButton!
    @IBOutlet weak var transferViewButton: UIView!
    @IBOutlet weak var buttonHistory: UIButton!
    @IBOutlet weak var historyViewButton: UIView!
    
    // Представления
    var transferView: TransferForm!
    var historyView: HistoryTable!
    
    // Прелоад
    var preload: PreloadView!
    
    // Данные пользователя
    private var userId: Int!
    private var userBalance: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureTransfer = UITapGestureRecognizer(target: self, action: #selector(self.setTransferForm(_:)))
        transferViewButton.addGestureRecognizer(tapGestureTransfer)
        // Добавил Дубль действия по причине неудачи распространения нажатия на представление в области кнопки
        buttonTransfer.addTarget(self, action: #selector(self.setTransferForm(_:)), for: .touchUpInside)
        
        let tapGestureHistory = UITapGestureRecognizer(target: self, action: #selector(self.setHistoryForm(_:)))
        historyViewButton.addGestureRecognizer(tapGestureHistory)
        // Добавил Дубль действия по причине неудачи распространения нажатия на представление в области кнопки
        buttonHistory.addTarget(self, action: #selector(self.setHistoryForm(_:)), for: .touchUpInside)
        
        self.preload = PreloadView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height), delegatView: view, animate: false)
            
        DispatchQueue.main.async {
            
            // Получаем данные пользователя
            Network().connect(method: "GET", actionType: "usersInfo", token: true) {results in

                if (results["status"] as! Bool) {
                    
                    let data = results["data"]?["user_info_token"] as? [String: AnyObject]
                    
                    // Обновляем данные в интерфейсе
                    let row = GetDataRow(data: data!)
                    self.balanceTitle.text = row.balance
                    self.nameTitle.text = row.name
                    
                    // Пишем используемые данные пользователя
                    self.userId = row.id
                    self.userBalance = row.balanceDouble
                    
                    self.setTransferFormParam()
                    
                    self.preload.removeFromSuperview()
                }else {
                    // Заменяем прелоад окном об ошибке
                    self.preload.setAlert(message: results["message"] as! String)
                }
            }
        }
        
    }
    
    private func setTransferFormParam(){
        
        self.transferView = TransferForm(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        self.underContentView.addSubview(self.transferView)
        
        let tapGestureRecipientSearch = UITapGestureRecognizer(target: self, action: #selector(self.recipientSearch(_:)))
        self.transferView.recipientField.addGestureRecognizer(tapGestureRecipientSearch)
        
        let tapGestureTransfer = UITapGestureRecognizer(target: self, action: #selector(self.transferAction(_:)))
        self.transferView.transferButton.addGestureRecognizer(tapGestureTransfer)
        
    }
    
    // Действие перевода средств
    @objc func transferAction(_ gesture: UIGestureRecognizer){
        
        guard (self.transferView != nil)
            else {
                return
        }
        guard let amount = transferView.amountField.text,
            let recipient = transferView.recipientField.text
            else {
                return
        }
        // Скрываем клавиатуру
        view.endEditing(true)
        // Запускаем прелоад
        self.preload = PreloadView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height), delegatView: self.view)
        // Валидация
        
        let msg = Validate().check(fields: ["amount": amount, "recipient": recipient, "balance": self.userBalance])
        if msg != "" {
            preload.setAlert(message: msg)
            return
        }
        // Делаем запрос
        Network().connect(method: "POST", actionType: "createTransaction", params: ["name": recipient, "amount": amount], token: true) {results in
            
            if (results["status"] as! Bool) {
                
                let data = results["data"]?["trans_token"] as? [String: AnyObject]
                
                let row = GetDataRow(data: data!)
                // Обновляем данные в интерфейсе
                self.balanceTitle.text = row.balance
                
                // Пишем используемые данные пользователя
                self.userBalance = row.balanceDouble
                
                // Чистим форму
                self.transferView.amountField.text = ""
                self.transferView.recipientField.text = ""
                
                self.preload.setAlert(message: NSLocalizedString("TRANSACTION_DONE", comment: ""))
            }else {
                // Заменяем прелоад окном об ошибке
                self.preload.setAlert(message: results["message"] as! String)
            }
            
        }
        
    }
    
    // Блокировка клавиатуры и запуск экрана поиска пользователя
    @objc func recipientSearch(_ gesture: UIGestureRecognizer){
        
        self.view.endEditing
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "UserSearchPage")
        self.present(controller, animated: true, completion: nil)
        
    }
    
    @IBAction func unwindToViewController (sender: UIStoryboardSegue){
        
        // Если это форма поиска пользователя, то устанавливаем значения
        if let userSearchView = sender.source as? UserSearchViewController {
            if userSearchView.userData.count > 0 && self.transferView != nil {
                self.transferView.recipientField.text = (userSearchView.userData["name"] as! String)
            }
        }
        
    }
    
    // Скрывает клавиатуру при любом нажатии на экран
    @IBAction func onTap(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    // Метод установки стиля StatusBar
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func exitAction(_ sender: UIButton) {
        
        UserDefaults.standard.removeObject(forKey: TOKEN)
        UserDefaults.standard.synchronize()
        
        self.view.backgroundColor = .white
        UIApplication.shared.statusBarView?.backgroundColor = ACTIVE_COLOR
        
        UIView.animate(withDuration: ANIMATE_DURATION, animations: {
            
            self.contentView.alpha = 0
            UIApplication.shared.statusBarView?.backgroundColor = ACTIVE_COLOR.withAlphaComponent(0.0)
            
        }, completion: { (value: Bool) in
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "LoginPage")
            self.present(controller, animated: true, completion: nil)
            
        })
        
    }
    
    // Показ представления Истории Переводов
    @objc func setHistoryForm(_ sender: UITapGestureRecognizer!) {

        view.endEditing(true)
        self.preload = PreloadView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height), delegatView: self.view)
        
        Network().connect(method: "GET", actionType: "listTransactions", token: true) {results in
            if (results["status"] as! Bool) {
                
                self.transferView = nil
                self.underContentView.removeAllSubViews()
                
                let data = results["data"]?["trans_token"] as? [[String : AnyObject]]
                
                let history = HistoryTable(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height), data: data!)
                history.delegate = self
                self.historyView = history
                self.underContentView.addSubview(self.historyView)
                
                self.preload.closed()
                
            }else {
                // Заменяем прелоад окном об ошибке
                self.preload.setAlert(message: results["message"] as! String)
            }
            
        }
        
    }
    
    // Показ представления Перевода средств
    @objc func setTransferForm(_ sender: UITapGestureRecognizer) {
        
        self.historyView = nil
        self.underContentView.removeAllSubViews()
        self.setTransferFormParam()
        
    }

}
