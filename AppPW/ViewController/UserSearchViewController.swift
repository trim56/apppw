//
//  UserSearchViewController.swift
//  AppPW
//
//  Created by Павел Зорин on 11/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

class UserSearchViewController: UIViewController {
    
    @IBOutlet weak var searchField: UISearchBar!
    @IBOutlet weak var preloadView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private let cellId = "userCell"
    private var data: [AnyObject] = []
    var userData: [String: AnyObject] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Настройка стиля поля Поиска
        let textFieldInsideSearchBar = searchField.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.placeholderColor(color: DISABLED_COLOR)
        textFieldInsideSearchBar?.textColor = TEXT_COLOR
        textFieldInsideSearchBar?.tintColor = ACTIVE_COLOR
        // Костылем избавляемся он рамок сверху и снизу поля
        searchField.layer.borderColor = ACTIVE_COLOR.cgColor
        searchField.layer.borderWidth = 1
        
        searchField.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    // Метод установки стиля StatusBar
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    // Запуск Прелоада возле строки поиска
    private func setPreload(){
        
        self.preloadView.removeAllSubViews()
        let spinnerView = JTMaterialSpinner(frame: CGRect(x: 7, y: 7, width: 30, height: 30))
        spinnerView.circleLayer.lineWidth = 2.0
        spinnerView.circleLayer.strokeColor = BUTTON_TEXT_COLOR.cgColor
        spinnerView.beginRefreshing()
        preloadView.addSubview(spinnerView)
        
    }

}

extension UserSearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Запрашиваем повторно используемую ячейку, что бы разместить в ней данные
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        // Если нет повторноиспользуемой ячейки, тогда создаем новую
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellId)
        }

        // Присваеваем текст ячейки через номер строки и получение строки из массива
        cell?.textLabel?.text = data[indexPath.row]["name"] as? String
        cell?.textLabel?.textColor = TEXT_COLOR
        cell?.textLabel?.font = UIFont(name: FONT_REGULAR, size: 15.0)
        
        return cell!
        
    }
    
    
}

extension UserSearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.userData = data[indexPath.row] as! [String : AnyObject]
        self.performSegue(withIdentifier: "setUserSeque", sender: self)
    }
    
}


extension UserSearchViewController: UISearchBarDelegate {
   
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText != "" {
            self.setPreload()
            
            Network().connect(method: "POST", actionType: "userList", params: ["filter": searchText], token: true) {results in
                if (results["status"] as! Bool) {
                    self.data = results["data"]?["list"] as! [AnyObject]
                    self.tableView.reloadData()
                }
                self.preloadView.removeAllSubViews()
            }
        }else {
            data = []
            tableView.reloadData()
        }
    }
}
