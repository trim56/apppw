//
//  TransferData.swift
//  AppPW
//
//  Created by Павел Зорин on 14/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

struct GetDataRow {
    private let data: [String: AnyObject]
    
    init(data: [String: AnyObject]) {
        self.data = data
    }
    
    var userName: String? {
        return data["username"] as? String
    }
    
    var date: String? {
        return (data["date"] as? String)!.formatToString(format: "dd.MM.yyyy HH:mm")
    }
    
    var amount: String? {
        return (data["amount"] as? NSNumber)?.moneyAmount
    }
    
    var absAmount: String? {
        return "\((data["amount"] as? Double)!.magnitude)"
    }
    
    var balance: String? {
        return (data["balance"] as? NSNumber)?.moneyAmount
    }
    
    var balanceDouble: String? {
        return "\(data["balance"] as? Double ?? 0.0)"
    }
    
    var name: String? {
        return data["name"] as? String
    }
    
    var id: Int? {
        return data["id"] as? Int
    }
    

}
