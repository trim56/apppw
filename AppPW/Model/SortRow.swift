//
//  SortRow.swift
//  AppPW
//
//  Created by Павел Зорин on 15/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

struct SortRow {
    private var resources: [[String: AnyObject]]!
    
    private let orderList:[String: ComparisonResult] = [
        "ASC": .orderedAscending,
        "DESC": .orderedDescending
    ]
    
    init(data: [[String: AnyObject]], field: String = "date", order: String = "ASC") {
        
        switch field {
        case "date":
            self.resources = data.sorted{ (value1, value2) -> Bool in
                let v1 = (value1[field] as! String).formatToDate()!
                let v2 = (value2[field] as! String).formatToDate()!
                return v1.compare(v2) == self.orderList[order]
            }
            break
        case "username":
            self.resources = data.sorted{ (value1, value2) -> Bool in
                let v1 = (value1[field] as! String)
                let v2 = (value2[field] as! String)
                return v1.compare(v2) == self.orderList[order]
            }
            break
        case "amount":
            self.resources = data.sorted{ (value1, value2) -> Bool in
                let v1 = (value1[field] as! NSNumber)
                let v2 = (value2[field] as! NSNumber)
                return v1.compare(v2) == self.orderList[order]
            }
            break
        default:
            self.resources = data
            break
        }
        
    }
    
    var orderResources: [[String: AnyObject]] {
        return self.resources
    }

    
    
}
