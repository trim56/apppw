//
//  Network.swift
//  AppPW
//
//  Created by Павел Зорин on 09/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

class Network {
    
    let url: String = "http://193.124.114.46:3001"
    let action: Dictionary = [
        "create": "users",
        "login": "sessions/create",
        "listTransactions": "api/protected/transactions",
        "createTransaction": "api/protected/transactions",
        "usersInfo": "api/protected/user-info",
        "userList": "api/protected/users/list",
    ]
    let errorMessage: [String:[Int:String]] = [
        "create": [
            400 : NSLocalizedString("NETWORK_CREATE_400", comment: ""),
            401 : NSLocalizedString("NETWORK_CREATE_401", comment: ""),
        ],
        "login": [
            400 : NSLocalizedString("NETWORK_LOGIN_400", comment: ""),
            401 : NSLocalizedString("NETWORK_LOGIN_401", comment: ""),
        ],
        "listTransactions": [
            401 : NSLocalizedString("NETWORK_UNAUTHORIZED_ERROR", comment: ""),
        ],
        "createTransaction": [
            400 : NSLocalizedString("NETWORK_CREATE_TRANSACTIONS_400", comment: ""),
            401 : NSLocalizedString("NETWORK_UNAUTHORIZED_ERROR", comment: ""),
        ],
        "usersInfo": [
            400 : NSLocalizedString("NETWORK_USER_INFO_400", comment: ""),
            401 : NSLocalizedString("NETWORK_UNAUTHORIZED_ERROR", comment: ""),
        ],
        "userList": [
            400 : "",
            401 : "",
        ]
    ]
    
    func connect(
        method: String,
        actionType: String,
        params: [String: String] = [:],
        token: Bool = false,
        completion: @escaping (Dictionary<String, AnyObject>
        ) -> Void){
        
        // Установка базовых значений параметров ответа
        var status: Bool = false
        var message: String = NSLocalizedString("NETWORK_UNAUTHORIZED_ERROR", comment: "")
        var json: Dictionary<String, AnyObject> = [:]
        
        // Конфигурация запроса
        let link = "\(url)/\(action[actionType] ?? "")"
        var request = URLRequest(url: URL(string: link)!)
        request.httpMethod = method
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // Если требуется передача данных в теле
        if params.count > 0 {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }
        
        // Если требуется авторизация по токену
        if token {
            if let key = UserDefaults.standard.object(forKey: TOKEN) {
                request.addValue("Bearer \(key)", forHTTPHeaderField: "Authorization")
            }else {
                return completion(["status": status, "message": NSLocalizedString("NETWORK_FAILED_KEY", comment: ""), "data": json] as Dictionary<String, AnyObject>)
            }
        }
        
        let session = URLSession.shared
        session.dataTask(with: request, completionHandler: { (data, response, error) in

            // Если получен ответ и код известен
            if let httpResponse = response as? HTTPURLResponse {
                let code = httpResponse.statusCode
                switch code {
                case 200...201:
                    
                    do {
                        
                        let anyData = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                        // Распознование уровня вложенности получаемых данных (учитывая, что известно только о двух видах, основываясь на API)
                        if anyData is [[String: AnyObject]] {
                            let array = anyData as! [[String: AnyObject]]
                            json = ["list": array] as Dictionary<String, AnyObject>
                        }else{
                            json = anyData as! Dictionary<String, AnyObject>
                        }
                        
                        status = true
                        message = "Ok"
                    } catch {
                        message = NSLocalizedString("NETWORK_FAILED_DATA", comment: "")
                    }
                    break
                default:
                    // Ищем текст ошибки и устанавливаем в сообщение
                    if let msg = self.errorMessage[actionType]![code] {
                        message = msg
                    }
                    
                    break
                }
                
            }
            
            // Возвращаемся в основной поток с данными
            DispatchQueue.main.async {
                completion(["status": status, "message": message, "data": json] as Dictionary<String, AnyObject>)
            }
            
        }).resume()
        
    }
    
}
