//
//  Constant.swift
//  AppPW
//
//  Created by Павел Зорин on 08/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

// Ключ токена
let TOKEN = "pw_token"

let ANIMATE_DURATION: Double = 0.3

// Цвет активности
let ACTIVE_COLOR: UIColor = UIColor(hex: "7CB342")!
// Цвет неактивности
let DISABLED_COLOR: UIColor = UIColor(hex: "CFD8DC")!
// Цвет по умолчанию
let DEFAULT_COLOR: UIColor = UIColor(hex: "BDBDBD")!
// Цвет текста кнопки
let BUTTON_TEXT_COLOR: UIColor = UIColor(hex: "F5F5F5")!
// Цвет текста в полях ввода
let TEXT_COLOR: UIColor = UIColor(hex: "1C313A")!

// Глобальные шрифты
let FONT_REGULAR: String = "Roboto-Regular"
let FONT_MEDIUM: String = "Roboto-Medium"
