//
//  PreloadView.swift
//  AppPW
//
//  Created by Павел Зорин on 08/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

class PreloadView: UIView {
    
    // Центральный блок прелоада
    let preloadBlock = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 120))
    
    convenience init(frame: CGRect, delegatView: UIView, animate: Bool = true) {
        self.init(frame: frame)
        commonInit()
        
        delegatView.addSubview(self)
        
        self.leadingAnchor.constraint(equalTo: delegatView.leadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: delegatView.trailingAnchor).isActive = true
        self.topAnchor.constraint(equalTo: delegatView.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: delegatView.bottomAnchor).isActive = true
        
        // Анимация для плавного появления
        if animate {
            self.alpha = 0
            UIView.animate(withDuration: ANIMATE_DURATION, animations: {
                self.alpha = 1
            })
        }
    }
    
    private func commonInit() {
        
        backgroundColor = .clear
        translatesAutoresizingMaskIntoConstraints = false
        
        // Рамытее подложки
        let blurEffect = UIBlurEffect(style: .regular)
        let blurredEffectView = UIVisualEffectView(effect: blurEffect)
        blurredEffectView.frame = self.bounds
        addSubview(blurredEffectView)
        
        preloadBlock.backgroundColor = .white
        preloadBlock.layer.shadowColor = DISABLED_COLOR.cgColor
        preloadBlock.layer.shadowOpacity = 1
        preloadBlock.layer.shadowOffset = .zero
        preloadBlock.layer.shadowRadius = 10
        preloadBlock.translatesAutoresizingMaskIntoConstraints = true
        preloadBlock.center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        preloadBlock.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        addSubview(preloadBlock)
        
        // Спинер
        let spinnerView = JTMaterialSpinner(frame: CGRect(x: 80, y: 20, width: 40, height: 40))
        spinnerView.circleLayer.lineWidth = 2.0
        spinnerView.circleLayer.strokeColor = ACTIVE_COLOR.cgColor
        spinnerView.beginRefreshing()
        preloadBlock.addSubview(spinnerView)
        
        // Текст под спинером
        let preloadLabel = UILabel(frame: CGRect(x: 0, y: 88, width: 200, height: 12))
        preloadLabel.font = UIFont(name: FONT_REGULAR, size: 12)
        preloadLabel.textAlignment = .center
        preloadLabel.text = NSLocalizedString("WAIT", comment: "")
        preloadLabel.textColor = DISABLED_COLOR
        preloadBlock.addSubview(preloadLabel)
        
        
    }
    
    func setAlert(message: String){
        
        preloadBlock.removeAllSubViews()
        
        preloadBlock.frame = CGRect(x: 0, y: 0, width: 280, height: 200)
        preloadBlock.center = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        preloadBlock.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        
        // Текст под спинером
        let alertLabel = UILabel(frame: CGRect(x: 10, y: 10, width: 260, height: 120))
        alertLabel.font = UIFont(name: FONT_REGULAR, size: 14)
        alertLabel.textAlignment = .center
        alertLabel.text = message
        alertLabel.textColor = DEFAULT_COLOR
        alertLabel.numberOfLines = 0
        preloadBlock.addSubview(alertLabel)
        
        let alertButton = UIButton(frame: CGRect(x: 80, y: 142, width: 120, height: 36))
        alertButton.setTitle(NSLocalizedString("OK", comment: ""), for: .normal)
        alertButton.setTitleColor(BUTTON_TEXT_COLOR, for: .normal)
        alertButton.backgroundColor = ACTIVE_COLOR
        alertButton.titleLabel?.font = UIFont(name: FONT_REGULAR, size: 14)
        alertButton.addTarget(self, action: #selector(closed), for: .touchUpInside)
        preloadBlock.addSubview(alertButton)
        
    }
    
    @objc func closed(){
        
        UIView.animate(withDuration: ANIMATE_DURATION, animations: {
            self.alpha = 0
        }, completion: { (value: Bool) in
            self.removeFromSuperview()
        })
        
    }

}
