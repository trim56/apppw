//
//  Validate.swift
//  AppPW
//
//  Created by Павел Зорин on 09/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

// Для нежного отношения к серверу
class Validate {
    
    var message: String = ""
    var error: [String] = []
    
    func check(fields: [String: String]) -> String {
        for (field, value) in fields {
            
            let valueClear = value.trimmingCharacters(in: .whitespacesAndNewlines)
            
            switch field {
            case "username":
                
                if valueClear.count == 0 {
                    error.append(NSLocalizedString("VALIDATE_USERNAME", comment: ""))
                }
                
                break
            case "email":
                // Если через больше одного значения в строке
                let strArray = valueClear.split(separator: " ")
                
                if strArray.count == 0{
                    error.append(NSLocalizedString("VALIDATE_EMAIL_SPECIFIED", comment: ""))
                    break
                }
                
                if !String(strArray[0]).isValidEmail(){
                    error.append(NSLocalizedString("VALIDATE_EMAIL_INCORRECT", comment: ""))
                }
                
                break
            case "password":
                
                if valueClear.count == 0 {
                    error.append(NSLocalizedString("VALIDATE_PASSWORD_SPECIFIED", comment: ""))
                    break
                }
                
                break
            case "confirmPassword":
                
                if let password = fields["password"] {
                    
                    if password.trimmingCharacters(in: .whitespacesAndNewlines) != valueClear {
                        error.append(NSLocalizedString("VALIDATE_PASSWORD_MATCH", comment: ""))
                    }
                    
                }
                
                break
            case "amount":
                
                let float = valueClear.stringToNumber
                if float <= 0 {
                    error.append(NSLocalizedString("VALIDATE_AMOUNT", comment: ""))
                }
                
                break
            case "recipient":
                
                if valueClear == "" {
                    error.append(NSLocalizedString("VALIDATE_RECIPIENT", comment: ""))
                }
                
                break
            case "balance":
                
                if let float = fields["amount"] {
                    if (valueClear.stringToNumber - float.stringToNumber) < 0 {
                        error.append(NSLocalizedString("VALIDATE_NO_MONEY", comment: ""))
                    }
                    
                }
                
                break
            default:
                break
            }
            
        }
        
        if error.count > 0 {
            message = error.joined(separator: "\n")
        }
        
        return message
    }
    
}
