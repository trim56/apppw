//
//  HistoryTable.swift
//  AppPW
//
//  Created by Павел Зорин on 12/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

protocol HistoryTableDelegate: class {
    func repeatTransfer(data: [String: String])
}
// Страница истории переводов (History.xib)
class HistoryTable: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var sortSegment: UISegmentedControl!
    @IBOutlet weak var orderSegment: UISegmentedControl!
    
    // Для изменения высоты при анимации
    var heightConstraintFilterView: NSLayoutConstraint!
    
    private let cellId = "historyCell"
    private var historyData: [[String : AnyObject]] = [[:]]
    // Статус отображения меню фильтра
    static private var toggle: Bool = true
    // Параметры ограничения высоты блока фильтров (для анимации)
    private let heightConstraint: [Bool: CGFloat] = [
        true: 0.0,
        false: 85.0
    ]
    // Поля сортировки
    private let sort: [String] = [
        "date",
        "amount",
        "username"
    ]
    // Направление сортировки
    private let order: [String] = [
        "ASC",
        "DESC"
    ]
    // Активные параметры сортировки
    static private var sortActive: Int = 0
    static private var orderActive: Int = 1
    
    weak var delegate: HistoryTableDelegate?
 
    
    convenience init(frame: CGRect, data: [[String : AnyObject]] = [[:]]) {
        self.init(frame: frame)
        historyData = SortRow(data: data, field: sort[HistoryTable.sortActive], order: order[HistoryTable.orderActive]).orderResources
        commonInit()
    }
    
    private func commonInit(){
        
        Bundle.main.loadNibNamed("History", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Инициируем ячейку
        let nib = UINib.init(nibName: "HistoryCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: cellId)
        
        tableView.dataSource = self
        
        // Устанавливаем текущее ограничение для блока фильтра
        self.heightConstraintFilterView = filterView.heightAnchor.constraint(equalToConstant: heightConstraint[HistoryTable.toggle]!)
        self.heightConstraintFilterView.isActive = true
        
        // Устанавливаем выбранные параметры сегментов
        sortSegment.selectedSegmentIndex = HistoryTable.sortActive
        orderSegment.selectedSegmentIndex = HistoryTable.orderActive
        
    }
    
    // Действие повтора операции перевода
    @objc func repeatButtonAction(_ sender: UIButton){
        
        let tag = sender.tag
        let row = GetDataRow(data: historyData[tag])
        let userName = row.userName!
        let amount = row.absAmount!
        delegate?.repeatTransfer(data: ["username": userName, "amount": amount])
        
    }
    
    // Скрыть/Показать блок фильтрации
    @IBAction func filterBlockToggle(_ sender: UIButton) {

        UIView.animate(withDuration: ANIMATE_DURATION, animations: {
            
            HistoryTable.toggle = !HistoryTable.toggle
            self.heightConstraintFilterView.constant = self.heightConstraint[HistoryTable.toggle]!
            self.layoutIfNeeded()
            
        })
    }
    
    // Изменение поля  сортировки
    @IBAction func sortFieldAction(_ sender: UISegmentedControl) {

        HistoryTable.sortActive = sender.selectedSegmentIndex
        self.setNewSort()
        
    }
    
    // Изменения направления сортировки
    @IBAction func orderByAction(_ sender: UISegmentedControl) {
        
        HistoryTable.orderActive = sender.selectedSegmentIndex
        self.setNewSort()
        
    }
    
    // Обновляет данные истории согласно условиям сортировки
    private func setNewSort(){
        // Вероятно следует вывести в отдельный поток, так как данных может быть очень много
        historyData = SortRow(data: historyData, field: sort[HistoryTable.sortActive], order: order[HistoryTable.orderActive]).orderResources
        tableView.reloadData()
    }
    
}



extension HistoryTable: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: cellId) as! HistoryTableCell
        
        let row = GetDataRow(data: historyData[indexPath.row])
        // Присваеваем текст ячейки через номер строки и получение строки из массива
        cell.userName?.text = row.userName
        cell.date?.text = row.date
        cell.amount?.text = row.amount
        cell.balance?.text = "\(NSLocalizedString("BALANCE", comment: "")): \(row.balance ?? "")"
        cell.reloadTransfer.tag = indexPath.row
        cell.reloadTransfer.addTarget(self, action: #selector(repeatButtonAction(_:)), for: .touchUpInside)
        
        return cell
        
    }
    
}

