//
//  TransferForm.swift
//  AppPW
//
//  Created by Павел Зорин on 10/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

// Страница формы  переводов (Transfer.xib)
class TransferForm: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var recipientField: UITextField!
    @IBOutlet weak var transferButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @IBAction func doneTap(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    
    
    private func commonInit(){
                
        Bundle.main.loadNibNamed("Transfer", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        amountField.setBottomBorder(color: DISABLED_COLOR)
        recipientField.setBottomBorder(color: DISABLED_COLOR)

        amountField.placeholderColor(color: DISABLED_COLOR)
        recipientField.placeholderColor(color: DISABLED_COLOR)
        
        recipientField.delegate = self
        amountField.delegate = self
        
    }
    
    
 
}

extension TransferForm: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {

        if textField == recipientField {
            textField.resignFirstResponder()
            self.endEditing(true)
            return
        }
        textField.setBottomBorder(color: ACTIVE_COLOR)
        
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        textField.setBottomBorder(color: DISABLED_COLOR)
        return true
    }
}
