//
//  SignInForm.swift
//  AppPW
//
//  Created by Павел Зорин on 06/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

// Блок формы входа в систему (SignIn.xib)
class SignInForm: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    @IBAction func doneTap(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    private func commonInit(){
        
        Bundle.main.loadNibNamed("SignIn", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        emailField.setBottomBorder(color: DISABLED_COLOR)
        passwordField.setBottomBorder(color: DISABLED_COLOR)
        
        emailField.placeholderColor(color: DISABLED_COLOR)
        passwordField.placeholderColor(color: DISABLED_COLOR)
        
    }
    
}
