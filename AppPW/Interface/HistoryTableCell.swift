//
//  HistoryTableCell.swift
//  AppPW
//
//  Created by Павел Зорин on 12/08/2019.
//  Copyright © 2019 Павел Зорин. All rights reserved.
//

import UIKit

// Ячейка таблицы истории переводов (HistoryCell.xib)
class HistoryTableCell: UITableViewCell {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var reloadTransfer: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
